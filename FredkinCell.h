// -------------
// FredkinCell.h
// -------------

#ifndef FredkinCell_h
#define FredkinCell_h

#include "AbstractCell.h"

class FredkinCell : public AbstractCell {
    private:
        int _age;
    public:
        FredkinCell(bool state);
        void evolve(int neighbors) override;
        char operator*() override;
};

#endif