// --------
// Cell.c++
// --------

#include "Cell.h"

// Constructors
Cell::Cell(bool state) { _cell = new FredkinCell(state); }
void Cell::revive() { _cell->revive(); }

// Functions
void Cell::evolve(int neighbors) {
    _cell->evolve(neighbors);
    if (**_cell == '2') { _cell = new ConwayCell(true); }
}

// Operators
char Cell::operator*() { return **_cell; }
bool Cell::operator==(bool rhs) { return *_cell == rhs; }