// --------
// Life.c++
// --------

#include "Life.h"
#include <assert.h>

using namespace std;

template <class T>
Life<T>::Life(int rows, int cols) : generation(0), population(0) {
    grid = vector<vector<T*>>(rows + 2, vector<T*>(cols + 2, nullptr));

    for (size_t r = 0; r < grid.size(); ++r) {
        for (size_t c = 0; c < grid[0].size(); ++c) {
            grid[r][c] = new T(false);
        }
    }
}

template <class T>
void Life<T>::addCell(int r, int c) {
    if (*grid[r + 1][c + 1] == false) {
        (grid[r + 1][c + 1])->revive();
        ++population;
    }
}

template <class T>
void Life<T>::advanceGeneration() {
    vector<vector<int>> n_grid = vector<vector<int>>(grid.size(), vector<int>(grid[0].size(), 0));
    int count = 0;
    for (size_t r = 1; r < grid.size() - 1; ++r) {
        for (size_t c = 1; c < grid[0].size() - 1; ++c) {
            // Get current number of neighbors
            int neighbors = 0;

            // Diagonals
            if (**grid[r][c] == '*' || **grid[r][c] == '.') {
                if (*grid[r - 1][c - 1] == true) ++neighbors;
                if (*grid[r - 1][c + 1] == true) ++neighbors;
                if (*grid[r + 1][c - 1] == true) ++neighbors;
                if (*grid[r + 1][c + 1] == true) ++neighbors;
            }

            // Cross
            if (*grid[r - 1][c    ] == true) ++neighbors;
            if (*grid[r    ][c - 1] == true) ++neighbors;
            if (*grid[r    ][c + 1] == true) ++neighbors;
            if (*grid[r + 1][c    ] == true) ++neighbors;

            // Save in neighbors grid
            n_grid[r][c] = neighbors;
        }
    }

    for (size_t r = 1; r < grid.size() - 1; ++r) {
        for (size_t c = 1; c < grid[0].size() - 1; ++c) {
            grid[r][c]->evolve(n_grid[r][c]);
            if (**grid[r][c] != '-' && **grid[r][c] != '.') ++count;
        }
    }
    population = count;
    ++generation;
}

template <class T>
string Life<T>::toString() {
    string result = "\nGeneration = ";
    result += to_string(generation);
    result += ", Population = ";
    result += to_string(population);
    result += ".\n";
    for (size_t r = 1; r < grid.size() - 1; ++r) {
        for (size_t c = 1; c < grid[0].size() - 1; ++c) {
            result += **grid[r][c];
        }
        result += "\n";
    }
    return result;
}

template class Life<ConwayCell>;
template class Life<FredkinCell>;
template class Life<Cell>;