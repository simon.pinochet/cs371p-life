// ------------------
// RunLifeFredkin.c++
// ------------------

#include "Life.h"

using namespace std;

int main() {
    int num_tests;
    cin >> num_tests;

    while (num_tests--) {
        string s;
        getline(cin, s);

        // Get grid size
        int rows, cols;
        cin >> rows >> cols;
        Life<FredkinCell> life (rows, cols);

        // Get number of cells
        int num_cells;
        cin >> num_cells;

        // Place cells
        for(int i = 0; i < num_cells; ++i) {
            int row, col;
            cin >> row >> col;
            life.addCell(row, col);
        }

        // Get number of generations and frequency to print
        int num_gens, frequency;
        cin >> num_gens >> frequency;

        // Advance generations
        cout << "*** Life<FredkinCell> " << rows << "x" << cols << " ***\n";
        cout << life.toString();
        for (int i = 1; i <= num_gens; ++i) {
            life.advanceGeneration();
            if(i % frequency == 0) cout << life.toString();
        }
        if (num_tests > 0) cout << '\n';
    }
    return 0;
}