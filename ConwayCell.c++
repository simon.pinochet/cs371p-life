// --------------
// ConwayCell.c++
// --------------

#include "ConwayCell.h"

ConwayCell::ConwayCell(bool state) : AbstractCell(state) {}

void ConwayCell::evolve(int neighbors) {
    if ((*this == true) && (neighbors < 2 || 3 < neighbors)) this->kill();
    else if ((*this == false) && (neighbors == 3)) this->revive();
}

char ConwayCell::operator*() {
    if(*this == true) return '*';
    else return '.';
}