// ----------------
// AbstractCell.c++
// ----------------

#include "AbstractCell.h"

AbstractCell::AbstractCell(bool state) : _state(state) {}
void AbstractCell::kill() { _state = false; }
void AbstractCell::revive() { _state = true; }
bool AbstractCell::operator==(bool rhs) { return _state == rhs; }