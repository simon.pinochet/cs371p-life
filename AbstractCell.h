// --------------
// AbstractCell.h
// --------------

#ifndef AbstractCell_h
#define AbstractCell_h

class AbstractCell {
    private:
        bool _state;
    public:
        // Constructors
        AbstractCell(bool state);
        virtual ~AbstractCell() {};

        // Functions
        virtual void kill();
        virtual void revive();
        virtual void evolve(int neighbors) = 0;

        // Operators
        virtual bool operator==(bool rhs);
        virtual char operator*() = 0;
}; 

#endif