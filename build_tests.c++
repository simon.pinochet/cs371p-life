#include <stdlib.h>     /* srand, rand */
#include <iostream>
#include <fstream> 
#include <ctime>

using namespace std;

int main () {
    srand( time( NULL ) );

    int num_tests = 5;
    cout << num_tests << "\n\n";
    while (num_tests--) {
        int rows = rand() % 40 + 1;
        int cols = rand() % 40 + 1;
        cout << rows << " " << cols << '\n';

        int num_cells = rand() % 200 + 1;
        cout << num_cells << '\n';

        for (int i = 0; i < num_cells; ++i) {
            int r = rand() % rows;
            int c = rand() % cols;
            cout << r << " " << c << '\n';
        }

        int num_gens = rand() % 10 + 1;
        int frequency = rand() % num_gens + 1;
        cout << num_gens << " " << frequency << "\n\n";
    }

    return 0;
}