// ------
// Cell.h
// ------

#ifndef Cell_h
#define Cell_h

#include "AbstractCell.h"
#include "ConwayCell.h"
#include "FredkinCell.h"

// ----
// Cell
// ----

class Cell {
    private:
        AbstractCell* _cell;
    public:
        Cell(bool state);
        void revive();
        void evolve(int neighbors);
        char operator*();
        bool operator==(bool rhs);
};

#endif