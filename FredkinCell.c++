// -------------
// FredkinCell.h
// -------------

#include "FredkinCell.h"

FredkinCell::FredkinCell(bool state) : AbstractCell(state), _age(0) {}

void FredkinCell::evolve(int neighbors) {
    if (*this == true)
        if (neighbors % 2 == 0) this->kill();
        else ++_age;
    else if ((*this == false) && (neighbors % 2 == 1)) this->revive();
}

char FredkinCell::operator*() {
    if(*this == true) {
        if (_age < 10) return '0' + _age;
        else return '+';
    } else return '-';
}