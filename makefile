 g.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

ifeq ($(shell uname -s), Life)
    ASTYLE        := astyle
    BOOST         := /usr/local/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-9
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main
    DOXYGEN       := doxygen
    GCOV          := gcov-9
    VALGRIND      := valgrind
else ifeq ($(shell uname -p), unknown)
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main -pthread
    DOXYGEN       := doxygen
    GCOV          := gcov
    VALGRIND      := valgrind
else
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-9
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main -pthread
    DOXYGEN       := doxygen
    GCOV          := gcov-9
    VALGRIND      := valgrind
endif

FILES :=                                  \
    .gitignore                            \
    life-tests                         \
    Life.c++                           \
    Life.h                             \
    makefile                              \
    RunLifeConway.c++                        \
	RunLifeFredkin.c++                        \
	RunLifeCell.c++                        \
    RunLifeConway.in                         \
    RunLifeConway.out                        \
	RunLifeFredkin.in                         \
    RunLifeFredkin.out                        \
	RunLifeCell.in                         \
    RunLifeCell.out                        \
    TestLife.c++						  \
    Life.log                           \
    html                                  

life-tests:
	git clone https://gitlab.com/gpdowning/cs371p-life-tests.git life-tests

html: Doxyfile Life.h
	$(DOXYGEN) Doxyfile

Life.log:
	git log > Life.log

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATEIC to YES
Doxyfile:
	$(DOXYGEN) -g

RunLifeConway: Life.h Life.c++ AbstractCell.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ RunLifeConway.c++
	-$(CPPCHECK) Life.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ AbstractCell.c++
	-$(CPPCHECK) RunLifeConway.c++
	$(CXX) $(CXXFLAGS) Life.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ AbstractCell.c++ RunLifeConway.c++ -o RunLifeConway

RunLifeConway.c++x: RunLifeConway
	./RunLifeConway < RunLifeConway.in > RunLifeConway.tmp
	-diff RunLifeConway.tmp RunLifeConway.out

RunLifeFredkin: Life.h Life.c++ AbstractCell.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ RunLifeFredkin.c++
	-$(CPPCHECK) Life.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ AbstractCell.c++
	-$(CPPCHECK) RunLifeFredkin.c++
	$(CXX) $(CXXFLAGS) Life.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ AbstractCell.c++ RunLifeFredkin.c++ -o RunLifeFredkin

RunLifeFredkin.c++x: RunLifeFredkin
	./RunLifeFredkin < RunLifeFredkin.in > RunLifeFredkin.tmp
	-diff RunLifeFredkin.tmp RunLifeFredkin.out

RunLifeCell: Life.h Life.c++ AbstractCell.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ RunLifeCell.c++
	-$(CPPCHECK) Life.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ AbstractCell.c++
	-$(CPPCHECK) RunLifeCell.c++
	$(CXX) $(CXXFLAGS) Life.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ AbstractCell.c++ RunLifeCell.c++ -o RunLifeCell

RunLifeCell.c++x: RunLifeCell
	./RunLifeCell < RunLifeCell.in > RunLifeCell.tmp
	-diff RunLifeCell.tmp RunLifeCell.out

TestLife: Life.h Life.c++ AbstractCell.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ TestLife.c++
	-$(CPPCHECK) Life.c++ AbstractCell.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++
	-$(CPPCHECK) TestLife.c++
	$(CXX) $(CXXFLAGS) Life.c++ AbstractCell.c++ ConwayCell.c++ FredkinCell.c++ Cell.c++ TestLife.c++ -o TestLife $(LDFLAGS)

DebugLife: Life.h Life.c++ TestLife.c++
	-cppcheck TestLife.c++
	-cppcheck Life.c++
	g++ -g -no-pie -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -Wall -Weffc++ -Wextra Life.c++ TestLife.c++ -o DebugLife -lgtest -lgtest_main -pthread

TestLife.c++x: TestLife
	$(VALGRIND) ./TestLife
	$(GCOV) -b Life.c++ | grep -A 5 "File '.*Life.c++'"
	$(GCOV) -b AbstractCell.c++ | grep -A 5 "File '.*AbstractCell.c++'"
	$(GCOV) -b ConwayCell.c++ | grep -A 5 "File '.*ConwayCell.c++'"
	$(GCOV) -b FredkinCell.c++ | grep -A 5 "File '.*FredkinCell.c++'"
	$(GCOV) -b Cell.c++ | grep -A 5 "File '.*Cell.c++'"

all: RunLife TestLife

check: $(FILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunLifeConway
	rm -f RunLifeFredkin
	rm -f RunLifeCell
	rm -f TestLife
	rm -f DebugLife

config:
	git config -l

ctd:
	$(CHECKTESTDATA) RunLife.ctd RunLifeConway.in
	$(CHECKTESTDATA) RunLife.ctd RunLifeFredkin.in
	$(CHECKTESTDATA) RunLife.ctd RunLifeCell.in

docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

format:
	$(ASTYLE) Life.c++
	$(ASTYLE) Life.h
	$(ASTYLE) RunLifeConway.c++
	$(ASTYLE) RunLifeFredkin.c++
	$(ASTYLE) RunLifeCell.c++
	$(ASTYLE) TestLife.c++

init:
	touch README
	git init
	git remote add origin git@gitlab.com:gpdowning/cs371p-life.git
	git add README.md
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Life.c++
	git add Life.h
	-git add Life.log
	-git add html
	git add makefile
	git add README.md
	git add RunLife.c++
	git add RunLife.ctd
	git add RunLifeConway.in
	git add RunLifeConway.out
	git add RunLifeFredkin.in
	git add RunLifeFredkin.out
	git add RunLifeCell.in
	git add RunLifeCell.out
	git add TestLife.c++
	git commit -m "another commit"
	git push
	git status

run: RunLifeConway.c++x RunLifeFredkin.c++x RunLifeCell.c++x TestLife.c++x

scrub:
	make clean
	rm -f  *.orig
	rm -f  Life.log
	rm -f  Doxyfile
	rm -rf life-tests
	rm -rf html
	rm -rf latex

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	make clean
	@pwd
	@rsync -r -t -u -v --delete            \
    --include "Life.c++"                \
    --include "Life.h"                  \
    --include "RunLifeConway.c++"             \
	--include "RunLifeFredkin.c++"             \
	--include "RunLifeCell.c++"             \
    --include "RunLife.ctd"             \
    --include "RunLifeConway.in"              \
    --include "RunLifeConway.out"             \
	--include "RunLifeFredkin.in"              \
    --include "RunLifeFredkin.out"             \
	--include "RunLifeCell.in"              \
	--include "RunLifeCell.out"              \
    --include "TestLife.c++"            \
    --exclude "*"                          \
    ~/projects/c++/life/ .
	@rsync -r -t -u -v --delete            \
    --include "makefile"                   \
    --include "Life.c++"                \
    --include "Life.h"                  \
    --include "RunLifeConway.c++"             \
	--include "RunLifeFredkin.c++"             \
	--include "RunLifeCell.c++"             \
    --include "RunLife.ctd"             \
    --include "RunLifeConway.in"              \
    --include "RunLifeConway.out"             \
	--include "RunLifeFredkin.in"              \
    --include "RunLifeFredkin.out"             \
	--include "RunLifeCell.in"              \
	--include "RunLifeCell.out"              \
    --include "TestLife.c++"            \
    --exclude "*"                          \
    . downing@$(CS):cs/git/cs371p-life/

versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% grep \"#define BOOST_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
ifneq ($(shell uname -s), Life)
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
