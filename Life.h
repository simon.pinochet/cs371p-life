// ------
// Life.h
// ------

#ifndef Life_h
#define Life_h

#include <vector>   // vector
#include <string>   // string
#include <iostream> // cin, cout

#include "AbstractCell.h"
#include "ConwayCell.h"
#include "FredkinCell.h"
#include "Cell.h"

template <class T>
class Life {
private:
    std::vector<std::vector<T*>> grid;
    int generation;
    int population;

public:
    Life(int rows, int cols);
    void addCell(int r, int c);
    void advanceGeneration();
    std::string toString();
};

#endif