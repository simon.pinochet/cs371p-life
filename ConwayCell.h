// ------------
// ConwayCell.h
// ------------

#ifndef ConwayCell_h
#define ConwayCell_h

#include "AbstractCell.h"

class ConwayCell : public AbstractCell {
    public:
        ConwayCell(bool state);
        void evolve(int neighbors) override;
        char operator*() override;
};

#endif