// ------------
// TestLife.c++
// ------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> //cin, cout

#include "gtest/gtest.h"

#include "Life.h"
using namespace std;

// --------------
// Abstract Tests
// --------------

// Test AbstractCell kill method
TEST(AbstractCellTest, test_kill) {
    AbstractCell* cell = new ConwayCell(true);
    ASSERT_EQ(*cell == true, true);

    cell->kill();
    ASSERT_EQ(*cell == true, false);
}

// Test AbstractCell revive method
TEST(AbstractCellTest, test_revive) {
    AbstractCell* cell = new ConwayCell(false);
    ASSERT_EQ(*cell == true, false);

    cell->revive();
    ASSERT_EQ(*cell == true, true);
}

// ------------
// Conway Tests
// ------------

// ConwayCell test 1
TEST(ConwayCellTest, test_1) {
    ConwayCell cell = ConwayCell(false);
    ASSERT_EQ(*cell, '.');

    cell.evolve(3);
    ASSERT_EQ(*cell, '*');
}

// ConwayCell test 2
TEST(ConwayCellTest, test_2) {
    ConwayCell cell = ConwayCell(true);
    ASSERT_EQ(*cell, '*');

    cell.evolve(0);
    ASSERT_EQ(*cell, '.');
}

// -------------
// Fredkin Tests
// -------------

// FredkinCell test 1
TEST(FredkinCellTest, test_3) {
    FredkinCell cell = FredkinCell(false);
    ASSERT_EQ(*cell, '-');

    cell.evolve(3);
    ASSERT_EQ(*cell, '0');

    for (int i = 0; i < 20; ++i) cell.evolve(3);
    ASSERT_EQ(*cell, '+');
}

// FredkinCell test 2
TEST(FredkinCellTest, test_4) {
    FredkinCell cell = FredkinCell(true);
    ASSERT_EQ(*cell, '0');

    cell.evolve(2);
    ASSERT_EQ(*cell, '-');
}

// ----------
// Cell Tests
// ----------

TEST(CellTest, test_5) {
    Cell cell = Cell(false);
    ASSERT_EQ(*cell, '-');
    ASSERT_EQ(cell == true, false);

    cell.revive();
    ASSERT_EQ(*cell, '0');
    ASSERT_EQ(cell == true, true);
}

TEST(CellTest, test_6) {
    Cell cell = Cell(true);
    ASSERT_EQ(*cell, '0');

    cell.evolve(3);
    cell.evolve(3);
    ASSERT_EQ(*cell, '*');

    cell.evolve(0);
    ASSERT_EQ(*cell, '.');
}

// ----------
// Life Tests
// ----------

TEST(LifeTest, test_7) {
    Life<ConwayCell> grid (2, 2);
    grid.addCell(0, 0);
    ASSERT_EQ(grid.toString(), "\nGeneration = 0, Population = 1.\n*.\n..\n");

    grid.advanceGeneration();
    ASSERT_EQ(grid.toString(), "\nGeneration = 1, Population = 0.\n..\n..\n");
}

TEST(LifeTest, test_8) {
    Life<FredkinCell> grid (2, 2);
    grid.addCell(0, 0);
    ASSERT_EQ(grid.toString(), "\nGeneration = 0, Population = 1.\n0-\n--\n");

    grid.advanceGeneration();
    ASSERT_EQ(grid.toString(), "\nGeneration = 1, Population = 2.\n-0\n0-\n");
}

TEST(LifeTest, test_9) {
    Life<Cell> grid (3, 3);
    grid.addCell(1, 1);
    ASSERT_EQ(grid.toString(), "\nGeneration = 0, Population = 1.\n---\n-0-\n---\n");

    grid.advanceGeneration();
    ASSERT_EQ(grid.toString(), "\nGeneration = 1, Population = 4.\n-0-\n0-0\n-0-\n");

    grid.addCell(1, 1);
    grid.advanceGeneration();
    ASSERT_EQ(grid.toString(), "\nGeneration = 2, Population = 4.\n-1-\n1-1\n-1-\n");

    grid.addCell(1, 1);
    grid.advanceGeneration();
    ASSERT_EQ(grid.toString(), "\nGeneration = 3, Population = 4.\n-*-\n*-*\n-*-\n");
}